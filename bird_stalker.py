#!/usr/bin/env python3

import os
import requests
import json
import random
import cv2
import imageio

from gpiozero import MotionSensor
from time import sleep, strftime
from picamera import PiCamera

# Generate colors for each object
verderon_macho = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255))
verderon_hembra = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255))
gorrion_macho = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255))
gorrion_hembra = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255))
# Get the api key and the url and check if they are set
_api_key = os.getenv("nanonets_api_key")
_url = os.getenv("nanonets_url")
if _api_key is not None:
    print("Api key ok")
else:
    print("Api key ERROR")
    exit(0)
if _url is not None:
    print("Url ok")
else:
    print("Url ERROR")
    exit(0)
# Set the pin where the motion sensor is connected
pir = MotionSensor(4)
# Set the images and predictions path and create them if they don't exist
path = "img/" + strftime("%Y-%m-%d") + "/"
os.makedirs(path, exist_ok=True)
predictions_path = path + "predictions/"
os.makedirs(predictions_path, exist_ok=True)
tmp_path = "/tmp/temp.jpeg"
# Initialize the camera
with PiCamera() as camera:
    print("Press Ctrl+C to exit")
    # Allow the camera to warm up
    sleep(0.5)
    print("Camera ready")
    date_str = strftime("%Y%m%d")
    try:
        # Main loop
        while True:
            print("Waiting for movement...")
            # Wait until motion is detected
            pir.wait_for_motion()
            print("Motion detected")
            # Create a string with the actual time
            time_str = strftime("%H%M%S")
            # Set the file name and the path
            files_name = date_str + "-" + time_str
            file_path = path + files_name
            # Capture a sequence
            camera.capture_sequence([
                file_path + "-%02d.jpeg" % i
                for i in range(10)
            ], format="jpeg")
            print("Sequence saved")
            print(files_name)
            # Make predictions on the images
            images = []
            for i in range(10):
                print("Making prediction " + str(i) + "...")
                img = file_path + "-%02d.jpeg" % i
                data = {'file': open(img, 'rb')}
                response = requests.post(_url, auth=requests.auth.HTTPBasicAuth(_api_key, ''), files=data)
                response = json.loads(response.text)
                print("Response: " + response["message"])
                if response["message"] == "Success":
                    print("Result: " + response["result"][0]["message"])
                    if response["result"][0]["message"] == "Success":
                        prediction = response["result"][0]["prediction"]
                        print("Prediction: " + str(prediction))
                        img_cv2 = None
                        for row in prediction:
                            # Check the label and set the color of the box
                            if row["label"] == "verderon_macho":
                                box_color = verderon_macho
                            elif row["label"] == "verderon_hembra":
                                box_color = verderon_hembra
                            elif row["label"] == "gorrion_macho":
                                box_color = gorrion_macho
                            elif row["label"] == "gorrion_hembra":
                                box_color = gorrion_hembra
                            # Draw boxes on the image
                            img_cv2 = cv2.imread(img)
                            cv2.rectangle(img_cv2, (row["xmin"], row["ymin"]), (row["xmax"], row["ymax"]), box_color, 1)
                        # Save the image in a temp file and add it into an array
                        if img_cv2 is not None:
                            cv2.imwrite(tmp_path, img_cv2)
                            images.append(imageio.imread(tmp_path))
                        # Delete temp file
                        cmd = "sudo rm " + tmp_path
                        os.system(cmd)
            print("Finished making predictions")
            # Saves a gif in the predictions directory
            print("Creating gif...")
            imageio.mimsave(predictions_path + files_name + ".gif", images, duration=0.3)
            print("Gif saved")
            images.clear()
            sleep(3)
    # Stop the program when pressing Ctrl+C
    except KeyboardInterrupt:
        print("Program cancelled")
