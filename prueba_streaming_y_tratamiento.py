#!/usr/bin/env python3

import json
import os
import random
import requests
import time

import cv2
import png
from picamera import PiCamera
# import the necessary packages
from picamera.array import PiRGBArray

# get the api key and the url
_api_key = os.getenv("nanonets_api_key")
_url = os.getenv("nanonets_url")

# generate colors for each object
verderon_macho = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255), 127)
verderon_hembra = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255), 127)
gorrion_macho = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255), 127)
gorrion_hembra = (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255), 127)

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))

# allow the camera to warmup
time.sleep(0.1)

# capture frames from the camera

for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):

    begin = time.clock()
    path = "/tmp/temp.png"
    png.from_array(frame.array, mode="RGB").save(path)
    img = open(path, "rb")

    # make a prediction on the image
    data = {'file': img}
    response = requests.post(_url, auth=requests.auth.HTTPBasicAuth(_api_key, ''), files=data)
    # print(response.text)

    # grab the raw NumPy array representing the image, then initialize the timestamp
    # and occupied/unoccupied text
    image = frame.array

    # draw boxes on the image
    response = json.loads(response.text)
    prediction = response["result"][0]["prediction"]
    # print(prediction)
    for i in prediction:
        cv2.rectangle(image, (i["xmin"], i["ymin"]), (i["xmax"], i["ymax"]),
                      (random.randint(1, 255), random.randint(1, 255), random.randint(1, 255)), thickness=3)

    # show the frame
    cv2.imshow("Bird Stalker", image)
    end = time.clock()
    elapsed_time = end - begin
    print(elapsed_time)
    key = cv2.waitKey(1) & 0xFF

    # clear the stream in preparation for the next frame
    rawCapture.truncate(0)

    time.sleep(10)

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        command = "sudo rm " + path
        os.system(command)
        break
