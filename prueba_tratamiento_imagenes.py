#!/usr/bin/env python3

import json
import os
import random
import requests

from PIL import Image, ImageDraw

img_path = "/home/pi/bird-stalker/img/2019-05-11/20190511-180049-00.jpeg"
_api_key = os.getenv("nanonets_api_key")
if _api_key is not None:
    print("Api key ok")
_url = os.getenv("nanonets_url")
if _url is not None:
    print("Url ok")
print(_url)

# make a prediction on the image
data = {'file': open(img_path, 'rb')}
response = requests.post(_url, auth=requests.auth.HTTPBasicAuth(_api_key, ''), files=data)
print(response.text)

# draw boxes on the image
response = json.loads(response.text)
im = Image.open(img_path)
draw = ImageDraw.Draw(im, mode="RGBA")
prediction = response["result"][0]["prediction"]
print(prediction)
for i in prediction:
    draw.rectangle((i["xmin"], i["ymin"], i["xmax"], i["ymax"]),
                   fill=(random.randint(1, 255), random.randint(1, 255), random.randint(1, 255), 127))
im.save("20190511-180049-00_PREDICTION.jpeg")
os.system("xdg-open 20190511-180049-00_PREDICTION.jpeg")
