#!/usr/bin/env python3

import os
import RPi.GPIO as GPIO
import time
from picamera import PiCamera
from google.cloud import storage

# inicializamos la cámara
camera = PiCamera()

# inicializamos cloud storage y especificamos el bucket
client = storage.Client()
bucket_name = "project-snow-white.appspot.com"
bucket = client.get_bucket(bucket_name)

# iniciamos el sensor de movimiento
pin = 14
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin, GPIO.IN)

# le damos tiempo al sensor para que empiece a funcionar
print ("Press Ctrl+C to exit")
# guardamos en un string la fecha del día para incluirla en el nombre 
# de la carpeta de la base de datos
datestr = time.strftime("%Y%m%d")
time.sleep(2)
print ("Ready")
try:
    # bucle principal
    while True:
        print("Waiting for movement...")
	# espera hasta que detecta una subida de tensión en el pin
        GPIO.wait_for_edge(pin, GPIO.RISING)
        print ("Motion detected")
        # creamos un string con la fecha y hora actual
        timestr = time.strftime("%Y%m%d-%H%M%S")
        fileName = timestr + ".jpeg"
        # guardamos una captura
        camera.capture(fileName, format="jpeg")
        # y la guardamos en cloud storage
        blob = bucket.blob("img-" + datestr + "/" + fileName)
        try:
            blob.upload_from_filename(fileName)
            print ("Capture saved")
            print (timestr)
        except ConnectionError:
            print("Error uploading image")
		
        time.sleep(10)
# para parar el programa pulsando Ctrl+C
except KeyboardInterrupt:
    print ("Program cancelled")
finally:
	# para asegurarnos que se liberan los recursos utilizados
	camera.close()
	GPIO.cleanup
