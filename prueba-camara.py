#!/usr/bin/env python3

import time
from picamera import PiCamera

camera = PiCamera()

print ("Press Ctrl+C to exit")

try:
    camera.rotation = 90
    # camera.start_preview()
    print ("Ready")
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    # camera.stop_preview()
    camera.capture("prueba", format="jpeg")
finally:
    camera.close()
	
