#!/usr/bin/env python3

from gpiozero import MotionSensor
from time import sleep, strftime
from picamera import PiCamera

# indicamos el pin donde está conectado el sensor de movimiento
pir = MotionSensor(4)
# inicializamos la cámara
with PiCamera() as camera:
    # le damos tiempo al sensor para que empiece a funcionar
    print("Press Ctrl+C to exit")
    # guardamos en un string la fecha del día para incluirla en el nombre
    # de la carpeta de la base de datos
    datestr = strftime("%Y%m%d")
    camera.framerate = 32
    print("Ready")
    try:
        # bucle principal
        while True:
            print("Waiting for movement...")
        # espera hasta que detecta movimiento
            pir.wait_for_motion()
            print("Motion detected")
            # creamos un string con la fecha y hora actual
            timestr = strftime("%Y%m%d-%H%M%S")
            fileName = timestr + ".jpeg"
            # guardamos una captura
            camera.capture(fileName, format="jpeg")
            print("Capture saved")
            print(timestr)
            sleep(3)
    # para parar el programa pulsando Ctrl+C
    except KeyboardInterrupt:
        print("Program cancelled")
